@extends('layout.master')
@section('title', 'Halaman Home')
@section('content')
<section class="content">
        
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Halaman Home</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <h1>Media Online</h1>
        <h2>Sosial media developer</h2>
        <p>belajar dan berbagi agar hidup menjadi lebih baik</p>
        <h2>Benefit join di media online</h2>
           <ul>
                <li>mendapatkan motivasi dari sesama para developer</li>
                <li>sharing knowlege</li>
                <li>dibuat oleh calon developer terbaik</li>
            </ul>
        <h2>cara bergabung ke media online</h2>
            <ol>
            <li>mengunjungi website ini</li>
            <li>mendaftar di <a href="/register"> Form Sign Up</a></li>
            <li>selesai</li>
            </ol>
        {{-- Start creating your amazing application! --}}
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        Footer
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>

@endsection

{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Media Online</h1>
<h2>Sosial media developer</h2>
    <p>belajar dan berbagi agar hidup menjadi lebih baik</p>
<h2>Benefit join di media online</h2>
    <ul>
    <li>mendapatkan motivasi dari sesama para developer</li>
    <li>sharing knowlege</li>
    <li>dibuat oleh calon developer terbaik</li>
    </ul>
<h2>cara bergabung ke media online</h2>
    <ol>
    <li>mengunjungi website ini</li>
    <li>mendaftar di <a href="/register"> Form Sign Up</a></li>
    <li>selesai</li>
    </ol>
</body>
</html> --}}