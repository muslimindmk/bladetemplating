@extends('layout.master')
@section('title', 'Halaman Form')
@section('content')
<section class="content">
            <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Halaman Form</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <h1>Buat account Baru</h1> 
        <h2>Sign up form</h2>
        <form action="/form-action" method="POST">
            @CSRF
            <label>First Name:</label><br>
            <input type="text" name="namadepan"><br>
            <label>LAst Name:</label><br>
            <input type="text" name="namabelakang"><br><br>
        
            <label>Gender</label><br>
            <input type="radio" name="sex" value="male"/>male<br>
            <input type="radio" name="sex" value="female"/>female<br><br>
           
            <label>Nationality</label><br>
            <select name="kebangsaan">
                <option value="wn">indonesia</option>
                <option value="wn">malaysia</option>
                <option value="wn">lainnya</option>
            </select><br><br>
        
            <label>LAnguage Spoken</label><br>
            <input type="checkbox" name="bhs">Bahasa indonesia<br>
            <input type="checkbox" name="bhs">Bahasa inggris<br>
            <input type="checkbox" name="bhs">lainnya<br><br>
            
            <label>Biodata</label><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br>
            
            <input type="submit" value="Sign up">
        </form>

        {{-- Start creating your amazing application! --}}
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        Footer
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>

@endsection



{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat account Baru</h1> 
<h2>Sign up form</h2>
<form action="/form-action" method="POST">
    @CSRF
    <label>First Name:</label><br>
    <input type="text" name="namadepan"><br>
    <label>LAst Name:</label><br>
    <input type="text" name="namabelakang"><br><br>

    <label>Gender</label><br>
    <input type="radio" name="sex" value="male"/>male<br>
    <input type="radio" name="sex" value="female"/>female<br><br>
   
    <label>Nationality</label><br>
    <select name="kebangsaan">
        <option value="wn">indonesia</option>
        <option value="wn">malaysia</option>
        <option value="wn">lainnya</option>
    </select><br><br>

    <label>LAnguage Spoken</label><br>
    <input type="checkbox" name="bhs">Bahasa indonesia<br>
    <input type="checkbox" name="bhs">Bahasa inggris<br>
    <input type="checkbox" name="bhs">lainnya<br><br>
    
    <label>Biodata</label><br>
    <textarea name="bio" rows="10" cols="30"></textarea><br>
    
    <input type="submit" value="Sign up">
</form>
</body>
</html> --}}