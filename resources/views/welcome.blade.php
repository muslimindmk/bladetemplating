@extends('layout.master')
@section('title', 'Halaman Index')
@section('content')
<section class="content">
        
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Halaman Index</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        
        <h1>SELAMAT DATANG! {{ $namadepan }} {{ $namabelakang }}</h1>
        <h3>terima kasih telah bergabung di website kami. Belajar kita Bersama!</h3>
        {{-- Start creating your amazing application! --}}
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        Footer
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>

@endsection


{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>SELAMAT DATANG! {{ $namadepan }} {{ $namabelakang }}</h1>
    <h3>terima kasih telah bergabung di website kami. Belajar kita Bersama!</h3>
</body>
</html> --}}